module Potepan::ProductDecorator
  def related_products
    Spree::Product.
      in_taxons(taxons).
      includes(master: [:images, :default_price]).
      distinct.
      where.not(id: id).
      order(:id)
  end

  Spree::Product.prepend self
end
