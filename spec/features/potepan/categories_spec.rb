require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxon) { create(:taxon, name: "Hogehoge", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: "Sample", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product_1) { create(:product, name: "Hogehoge-Bag", taxons: [taxon]) }
  let!(:product_2) { create(:product, name: "Hogehoge-Tshirt", taxons: [taxon]) }
  let!(:product_3) { create(:product, name: "Sample-Bag", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "shows the correct category page" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_content "Hogehoge-Bag"
    expect(page).to have_content "Hogehoge-Tshirt"
    expect(page).not_to have_content "Sample-Bag"
    expect(page).to have_content product_1.display_price.to_s
    expect(page).to have_content product_2.display_price.to_s
  end

  context "when User clicks the link in main content" do
    it "moves to the product page and can return to the previous page from the product page" do
      click_link product_1.name
      expect(current_path).to eq potepan_product_path(product_1.id)
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(product_1.taxons.first.id)
    end
  end

  it "shows taxonomy & taxon name in product-category(sidebar)" do
    within("#sidebar-category") do
      expect(page).to have_content 'Brand'
      expect(page).to have_content "Hogehoge (#{taxon.all_products.count})"
      expect(page).to have_content "Sample (#{other_taxon.all_products.count})"
      click_link 'Hogehoge'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  context "when User clicks the link in product-category(sidebar)" do
    it "moves to the correct category page" do
      within("#sidebar-category") do
        click_link 'Hogehoge'
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end
end
