require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "shows the related products" do
    within("#related_products") do
      expect(page).to have_content related_products.first.name
      expect(page).to have_content related_products.first.display_price.to_s
      expect(page).to have_content related_products.first.display_image.attachment_file_name
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  context "when User clicks the link in related-products(page_under)" do
    it "moves to the correct category page" do
      within("#related_products") do
        click_link related_products.first.name
      end
      expect(current_path).to eq potepan_product_path(related_products.first.id)
    end
  end
end
