require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    subject { full_title(page_title) }

    context 'when page_title is nil' do
      let(:page_title) { nil }

      it { expect(subject).to eq("BIGBAG Store") }
    end

    context 'when page_title is empty' do
      let(:page_title) { "" }

      it { expect(subject).to eq("BIGBAG Store") }
    end

    context 'when page_title is hogehoge' do
      let(:page_title) { "hogehoge" }

      it { expect(subject).to eq("hogehoge - BIGBAG Store") }
    end
  end
end
