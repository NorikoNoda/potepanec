require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe 'Get request' do
    let(:product) { create(:product, taxons: [create(:taxon)]) }

    before do
      get potepan_product_path(product.id)
    end

    it "http request is 200 success" do
      expect(response).to have_http_status(:success)
    end

    it "product name is included" do
      expect(response.body).to include product.name
    end

    it "product price is included" do
      expect(response.body).to include product.display_price.to_s
    end

    it "product description is included" do
      expect(response.body).to include product.description
    end
  end
end
