require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "Get #show" do
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'render show templete' do
      expect(response).to render_template("categories/show")
    end

    # インスタンス変数が期待される値を持つこと
    it '@taxon has the expected value' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@products has the expected value' do
      expect(assigns(:products)).to match_array(product)
    end
  end
end
