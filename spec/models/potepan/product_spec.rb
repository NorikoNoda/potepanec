require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "#related_products" do
    let(:taxon) { create(:taxon, name: "Hogehoge", parent: taxonomy.root, taxonomy: taxonomy) }
    let(:other_taxon) { create(:taxon, name: "Sample", taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, name: "Hogehoge-Bag", taxons: [taxon]) }
    let!(:related_items) { create_list(:product, 5, name: "Hogehoge-Tshirt", taxons: [taxon]) }
    let!(:unrelated_items) { create(:product, name: "Sample-Tshirt", taxons: [other_taxon]) }

    it "gets the related products" do
      expect(product.related_products).to eq related_items
    end
  end
end
